package decamp.s20;
import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class s20controller {


    @CrossOrigin
	@GetMapping("/reverseString")
    public  String reverse(@RequestParam String str) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();

        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }

        return strBuilder.toString();
    }

    @CrossOrigin
	@GetMapping("/ispalindrome")
        public  boolean isPalindrome3(@RequestParam String input) {
        if (input == null) {
            return false;
        }
        for(int i = 0; i < input.length(); i++) {
            if (input.charAt(i) != input.charAt(input.length() - 1 - i))
                return false;
        }
        return true;
    }

    @CrossOrigin
	@GetMapping("/removeDuplicateChar")
        public static String removeDuplicateChar(@RequestParam String str) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < str.length(); i++) {
            if (!charsPresent.contains(str.charAt(i))) {
                stringBuilder.append(str.charAt(i));
                charsPresent.add(str.charAt(i));
            }
        }

        return stringBuilder.toString();
    }

    @CrossOrigin
	@GetMapping("/minCat")
        public  String minCat(@RequestParam String st1,@RequestParam String st2)
    {
        if (st1.length() == st2.length())
            return st1+st2;
        if (st1.length() > st2.length())
        {
            int diff = st1.length() - st2.length();
            return st1.substring(diff, st1.length()) + st2;
        } else
        {
            int diff = st2.length() - st1.length();
            return st1 + st2.substring(diff, st2.length());
        }
    }
}
