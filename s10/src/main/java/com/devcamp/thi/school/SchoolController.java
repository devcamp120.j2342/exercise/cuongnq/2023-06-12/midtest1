package com.devcamp.thi.school;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchoolController {



    @CrossOrigin
	@GetMapping("/schools")
	public ArrayList<School> getSchool(){

		 ArrayList<Classroom> classrooms1 = new ArrayList<Classroom>();
          Classroom classroom1 = new Classroom(1, "lớp 1", 45);
         Classroom classroom2 = new Classroom(2, "lớp 2", 35);
          Classroom classroom3 = new Classroom(3, "lớp 3", 50);
        classrooms1.add( classroom1);
        classrooms1.add( classroom2);
        classrooms1.add( classroom3);

         ArrayList<Classroom> classrooms2 = new ArrayList<Classroom>();
          Classroom classroom4 = new Classroom(4, "lớp 4", 43);
         Classroom classroom5 = new Classroom(5, "lớp 5", 35);
          Classroom classroom6 = new Classroom(6, "lớp 6", 35);
        classrooms2.add( classroom4);
        classrooms2.add( classroom5);
        classrooms2.add( classroom6);

         ArrayList<Classroom> classrooms3 = new ArrayList<Classroom>();
          Classroom classroom7 = new Classroom(7, "lớp 7", 45);
         Classroom classroom8 = new Classroom(8, "lớp 8", 24);
          Classroom classroom9 = new Classroom(9, "lớp 9", 54);
        classrooms3.add( classroom7);
        classrooms3.add( classroom8);
        classrooms3.add( classroom9);

        ArrayList<School> schools = new ArrayList<School>();
        School school1 = new School(1, "Quốc học huế", "huế", classrooms3);
         School school2 = new School(2, "Lương Thế Vinh", "Hà nội", classrooms2);
          School school3 = new School(1, "Lê Qúy Đôn", "Sài Gòn", classrooms1);

            schools.add(school1);
            schools.add(school2);
            schools.add(school3);
        return schools;    
	}
	

    @CrossOrigin
	@GetMapping("/classroom")
    	public School getClassroom(@RequestParam int schoolid){
        ArrayList<Classroom> classrooms1 = new ArrayList<Classroom>();
          Classroom classroom1 = new Classroom(1, "lớp 1", 45);
         Classroom classroom2 = new Classroom(2, "lớp 2", 35);
          Classroom classroom3 = new Classroom(3, "lớp 3", 50);
        classrooms1.add( classroom1);
        classrooms1.add( classroom2);
        classrooms1.add( classroom3);

         ArrayList<Classroom> classrooms2 = new ArrayList<Classroom>();
          Classroom classroom4 = new Classroom(4, "lớp 4", 43);
         Classroom classroom5 = new Classroom(5, "lớp 5", 35);
          Classroom classroom6 = new Classroom(6, "lớp 6", 35);
        classrooms2.add( classroom4);
        classrooms2.add( classroom5);
        classrooms2.add( classroom6);

         ArrayList<Classroom> classrooms3 = new ArrayList<Classroom>();
          Classroom classroom7 = new Classroom(7, "lớp 7", 45);
         Classroom classroom8 = new Classroom(8, "lớp 8", 24);
          Classroom classroom9 = new Classroom(9, "lớp 9", 54);
        classrooms3.add( classroom7);
        classrooms3.add( classroom8);
        classrooms3.add( classroom9);


        School school1 = new School(1, "Quốc học huế", "huế", classrooms3);
        School school2 = new School(2, "Lương Thế Vinh", "Hà nội", classrooms2);
        School school3 = new School(1, "Lê Qúy Đôn", "Sài Gòn", classrooms1);

        if (school1.getId() == schoolid) {
            return school1;
        }else if (school2.getId() == schoolid) {
            return school2;
        }else {
            return school3;
        }
    }


        @CrossOrigin
	@GetMapping("/classrooms")
	public ArrayList<Classroom> getTotalClassroom(){

		 ArrayList<Classroom> classrooms1 = new ArrayList<Classroom>();
          Classroom classroom1 = new Classroom(1, "lớp 1", 45);
         Classroom classroom2 = new Classroom(2, "lớp 2", 35);
          Classroom classroom3 = new Classroom(3, "lớp 3", 50);
        classrooms1.add( classroom1);
        classrooms1.add( classroom2);
        classrooms1.add( classroom3);

         ArrayList<Classroom> classrooms2 = new ArrayList<Classroom>();
          Classroom classroom4 = new Classroom(4, "lớp 4", 43);
         Classroom classroom5 = new Classroom(5, "lớp 5", 35);
          Classroom classroom6 = new Classroom(6, "lớp 6", 35);
        classrooms2.add( classroom4);
        classrooms2.add( classroom5);
        classrooms2.add( classroom6);

         ArrayList<Classroom> classrooms3 = new ArrayList<Classroom>();
          Classroom classroom7 = new Classroom(7, "lớp 7", 45);
         Classroom classroom8 = new Classroom(8, "lớp 8", 24);
          Classroom classroom9 = new Classroom(9, "lớp 9", 54);
        classrooms3.add( classroom7);
        classrooms3.add( classroom8);
        classrooms3.add( classroom9);

        ArrayList<School> schools = new ArrayList<School>();
        School school1 = new School(1, "Quốc học huế", "huế", classrooms3);
         School school2 = new School(2, "Lương Thế Vinh", "Hà nội", classrooms2);
          School school3 = new School(1, "Lê Qúy Đôn", "Sài Gòn", classrooms1);

            schools.add(school1);
            schools.add(school2);
            schools.add(school3);
         ArrayList<Classroom> classroomstotal = new ArrayList<Classroom>();   
         for (School school : schools) {
           for (Classroom classroom : school.getClassrooms()) {
            classroomstotal.add(classroom);
           }
         }
        return classroomstotal;
        }


                @CrossOrigin
	@GetMapping("/classroombig")
	public ArrayList<Classroom> getClassroomBig(@RequestParam int noNumber){

		 ArrayList<Classroom> classrooms1 = new ArrayList<Classroom>();
          Classroom classroom1 = new Classroom(1, "lớp 1", 45);
         Classroom classroom2 = new Classroom(2, "lớp 2", 35);
          Classroom classroom3 = new Classroom(3, "lớp 3", 50);
        classrooms1.add( classroom1);
        classrooms1.add( classroom2);
        classrooms1.add( classroom3);

         ArrayList<Classroom> classrooms2 = new ArrayList<Classroom>();
          Classroom classroom4 = new Classroom(4, "lớp 4", 43);
         Classroom classroom5 = new Classroom(5, "lớp 5", 35);
          Classroom classroom6 = new Classroom(6, "lớp 6", 35);
        classrooms2.add( classroom4);
        classrooms2.add( classroom5);
        classrooms2.add( classroom6);

         ArrayList<Classroom> classrooms3 = new ArrayList<Classroom>();
          Classroom classroom7 = new Classroom(7, "lớp 7", 45);
         Classroom classroom8 = new Classroom(8, "lớp 8", 24);
          Classroom classroom9 = new Classroom(9, "lớp 9", 54);
        classrooms3.add( classroom7);
        classrooms3.add( classroom8);
        classrooms3.add( classroom9);

        ArrayList<School> schools = new ArrayList<School>();
        School school1 = new School(1, "Quốc học huế", "huế", classrooms3);
         School school2 = new School(2, "Lương Thế Vinh", "Hà nội", classrooms2);
          School school3 = new School(1, "Lê Qúy Đôn", "Sài Gòn", classrooms1);

            schools.add(school1);
            schools.add(school2);
            schools.add(school3);
         ArrayList<Classroom> classroomsBig = new ArrayList<Classroom>();   
         for (School school : schools) {
           for (Classroom classroom : school.getClassrooms()) {
            if( classroom.getNoStudent() > noNumber) {
                classroomsBig.add(classroom);
            }
           }
         }
        return classroomsBig;
        }


    @CrossOrigin
	@GetMapping("/schoolbig")
	public ArrayList<School> getSchoolBig(@RequestParam int noNumber){

		 ArrayList<Classroom> classrooms1 = new ArrayList<Classroom>();
          Classroom classroom1 = new Classroom(1, "lớp 1", 45);
         Classroom classroom2 = new Classroom(2, "lớp 2", 35);
          Classroom classroom3 = new Classroom(3, "lớp 3", 50);
        classrooms1.add( classroom1);
        classrooms1.add( classroom2);
        classrooms1.add( classroom3);

         ArrayList<Classroom> classrooms2 = new ArrayList<Classroom>();
          Classroom classroom4 = new Classroom(4, "lớp 4", 43);
         Classroom classroom5 = new Classroom(5, "lớp 5", 35);
          Classroom classroom6 = new Classroom(6, "lớp 6", 35);
        classrooms2.add( classroom4);
        classrooms2.add( classroom5);
        classrooms2.add( classroom6);

         ArrayList<Classroom> classrooms3 = new ArrayList<Classroom>();
          Classroom classroom7 = new Classroom(7, "lớp 7", 45);
         Classroom classroom8 = new Classroom(8, "lớp 8", 24);
          Classroom classroom9 = new Classroom(9, "lớp 9", 54);
        classrooms3.add( classroom7);
        classrooms3.add( classroom8);
        classrooms3.add( classroom9);

        ArrayList<School> schools = new ArrayList<School>();
        School school1 = new School(1, "Quốc học huế", "huế", classrooms3);
         School school2 = new School(2, "Lương Thế Vinh", "Hà nội", classrooms2);
          School school3 = new School(1, "Lê Qúy Đôn", "Sài Gòn", classrooms1);

            schools.add(school1);
            schools.add(school2);
            schools.add(school3);
         ArrayList<School> schoolBig = new ArrayList<School>();   
         for (School school : schools) {
            if (school.getTotalStudent() > noNumber) {
                schoolBig.add(school);
            }
           }
         
        return schoolBig;
        }


 }
