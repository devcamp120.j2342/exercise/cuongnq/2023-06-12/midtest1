package com.devcamp.thi.school;
import java.util.ArrayList;

public class SchoolList {
    ArrayList<School> Schools = new ArrayList<School>();

	public ArrayList<School> getSchools() {
		return Schools;
	}

	public void setSchools(ArrayList<School> Schools) {
		this.Schools = Schools;
	}

    @Override
    public String toString() {
        return "SchoolList [School=" + Schools + "]";
    }
}
